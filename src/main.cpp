/*

    Project: Typing Practice

    Author: Treborsky, rob.koziarski2k@gmail.com

    Name: main.cpp

    Description: Entry point for the application.

*/
#include <iostream>
#include <vector>

class App {
    public:
        App() : words_to_type({"Easily", "typable", "text", "for", "beginners!"}), max_score(words_to_type.size()) {
            typed_words.reserve(words_to_type.size());
            std::cout << "Type the last word that appears in the console to score a point. You can make 3 mistakes." << std::endl;
            std::cout << "The maximum score for this text is: " << max_score << std::endl;
        }

        bool run() {
            for (const auto& word : words_to_type) {
                if (word != words_to_type[0]) {
                    output_buffer += " " + word;
                }
                else {
                    output_buffer += word;
                }
                while (input_buffer != word && !try_counter) { // accept input while the word hasn't been typed correctly
                    std::cout << output_buffer << std::endl;
                    ++try_counter;
                    std::cin >> input_buffer;  // type & accept by hitting enter
                }
                if (input_buffer == word) {
                    score += 1;
                }
                try_counter = 0;
            }

            if (score == max_score) {
                std::cout << "\nYou've hit all words perfectly!" << std::endl;
            } else {
                std::cout << "\nYou almost had it" << std::endl;
            } 
            std::cout << "Your score was: " << score << "/" << max_score << std::endl;
            return true;
        }

    private:
        const std::vector<std::string> words_to_type;
        const int max_score;
        std::vector<std::string> typed_words;
        std::string output_buffer = "";
        std::string input_buffer = "";
        int try_counter = 0;
        int score = 0;
};

int main () {
    App game;
    game.run();
}
