# TypingPractice
I really needed to code something fast in C++, so I did this thing. It did take 10 minutes, so nothing crazy.

## Plans
I plan to implement timing and some basic statistics. I might have to change the input/output thing and add tests and benchmarks (I just wanna learn them a bit).

## TODO:
- [ ] timing,
- [ ] stats,
- [ ] more user friendly input/output,
- [ ] tests,
- [ ] benchmarks.

After I've done that, I will release it as a package maybe - I don't know how it's done so it'll be good practice.
